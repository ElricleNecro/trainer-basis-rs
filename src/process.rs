//! This module provides tooling to interact with another process.

use std::fs::{self, File, OpenOptions};
use std::io::{self, Read, Seek, SeekFrom, Write};
use std::path::{Path, PathBuf};
use std::convert::TryFrom;

use crate::utils::search;

/// The goal of this structure is to summarise all useful information about a process.
#[derive(Debug)]
pub struct Process {
    /// - pid of the processus,
    pid: u32,
    /// - file reference on the process memory file,
    file: File,
    /// - path to the process directory in /proc,
    directory: PathBuf,

    /// - base address of the chosen memory module.
    base_addr: u64,
}

impl Process {
    /// Attach to the first process found with a name matching `prg_name`. By default, it load the
    /// first `r-xp` part of the memory.
    pub fn new(prg_name: &str) -> io::Result<Self> {
        let (pid, entry) = search(Path::new("/proc"), prg_name)?;
        let mut mem_path = entry.clone();
        mem_path.push("mem");

        let mut obj = Self {
            pid,
            file: OpenOptions::new().write(true).read(true).open(mem_path)?,
            directory: entry,

            base_addr: 0,
        };

        obj.set_module_base("r-xp")?;

        Ok(obj)
    }

    /// Same as `Process::new`, but allowing the user to chose the part of memory to explore.
    pub fn new_for_module(prg_name: &str, module: &str) -> io::Result<Self> {
        let (pid, entry) = search(Path::new("/proc"), prg_name)?;
        let mut mem_path = entry.clone();
        mem_path.push("mem");

        let mut obj = Self {
            pid,
            file: OpenOptions::new().write(true).read(true).open(mem_path)?,
            directory: entry,

            base_addr: 0,
        };

        obj.set_module_base(module)?;

        Ok(obj)
    }

    /// Attach to the process with the given pid. By default, it load the
    /// first `r-xp` part of the memory.
    pub fn from_pid(pid: u32) -> io::Result<Self> {
        let mut entry = Path::new("/proc").to_path_buf();
        entry.push(format!("{}", pid));

        let mut mem_path = entry.clone();
        mem_path.push("mem");

        let mut obj = Self {
            pid,
            file: OpenOptions::new().write(true).read(true).open(mem_path)?,
            directory: entry,

            base_addr: 0,
        };

        obj.set_module_base("r-xp")?;

        Ok(obj)
    }

    /// Same as `Process::from_pid`, but allowing the user to chose the part of memory to explore.
    pub fn from_pid_for_module(pid: u32, module: &str) -> io::Result<Self> {
        let mut entry = Path::new("/proc").to_path_buf();
        entry.push(format!("{}", pid));

        let mut mem_path = entry.clone();
        mem_path.push("mem");

        let mut obj = Self {
            pid,
            file: OpenOptions::new().write(true).read(true).open(mem_path)?,
            directory: entry,

            base_addr: 0,
        };

        obj.set_module_base(module)?;

        Ok(obj)
    }

    /// Switch between memory parts, at user discretion.
    pub fn set_module_base(&mut self, module: &str) -> io::Result<()> {
        self.base_addr = self.find_base_address(module)?;

        Ok(())
    }

    fn find_base_address(&self, module: &str) -> io::Result<u64> {
        let mut maps_path = self.directory.clone();
        maps_path.push("maps");

        let buffer = fs::read_to_string(maps_path)?;

        let mut offset: Vec<u64> = Vec::new();
        for line in buffer.split('\n') {
            if ! line.contains(module) {
                continue;
            }

            let addr = line.split('-').nth(0).ok_or_else(||
                io::Error::new(
                    io::ErrorKind::Other,
                    format!("Badly formatted line: '{}'.", line)
                )
            )?;

            offset.push(
                    u64::from_str_radix(addr, 16)
                        .map_err(|e| io::Error::new(io::ErrorKind::Other, format!("{:?}", e)))?
            );
        }

        Ok(*offset.get(0).ok_or_else(|| io::Error::new(io::ErrorKind::Other, "Did not find any valid address."))?)
    }

    /// Write the content of `buffer` starting at address `addr`.
    pub fn write(&mut self, addr: u64, buffer: &[u8]) -> io::Result<()> {
        self.file.seek(SeekFrom::Start(addr))?;

        self.file.write_all(buffer)?;

        Ok(())
    }

    /// Read memory starting at `addr` and fill `buffer`.
    pub fn read(&mut self, addr: u64, buffer: &mut [u8]) -> io::Result<()> {
        self.file.seek(SeekFrom::Start(addr))?;

        self.file.read_exact(buffer)?;

        Ok(())
    }

    /// Search the memory for `signature` using memory chunk of size `bsize`. If a `payload` is
    /// given, it will be writen in the memory at `found_addr + sigoffset`.
    pub fn payload(&mut self, signature: &[u8], payload: Option<&[u8]>, bsize: u64, sigoffset: u64) -> io::Result<()> {
        let offset = signature.len() * bsize as usize;
        let mut buffer = vec![0; offset];
        let mut idx = 0;

        'main: while let Ok(()) = self.read(self.base_addr + idx, &mut buffer) {
            for inner_idx in 0..=offset - signature.len() {
                if signature == &buffer[inner_idx..] {
                    if let Some(ref pl) = payload {
                        self.write(self.base_addr + idx + inner_idx as u64 + sigoffset, &pl)?;
                    }

                    break 'main;
                }
            }

            idx += offset as u64;
        }

        Ok(())
    }
}

impl TryFrom<u32> for Process {
    type Error = io::Error;

    fn try_from(from: u32) -> io::Result<Self> {
        Self::from_pid(from)
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryInto;
    use super::*;

    #[test]
    fn load_from_name() {
        let pid = std::process::id();
        let res_process = Process::new("hack");
        assert_eq!(true, res_process.is_ok());

        let process = res_process.unwrap();

        assert_eq!(pid, process.pid);
        assert_eq!(Path::new(&format!("/proc/{}", pid)), process.directory);
        assert_ne!(0, process.base_addr);
    }

    #[test]
    fn load_from_name_with_module() {
        let pid = std::process::id();
        let no_module = Process::new("hack").unwrap();
        let res_process = Process::new_for_module("hack", "r-xp");
        assert_eq!(true, res_process.is_ok());

        let process = res_process.unwrap();

        assert_eq!(pid, process.pid);
        assert_eq!(Path::new(&format!("/proc/{}", pid)), process.directory);
        assert_ne!(0, process.base_addr);
        assert_eq!(no_module.base_addr, process.base_addr);
    }

    #[test]
    fn load_from_id() {
        let pid = std::process::id();
        let res_process = Process::from_pid(std::process::id());
        assert_eq!(true, res_process.is_ok());

        let process = res_process.unwrap();

        assert_eq!(pid, process.pid);
        assert_eq!(Path::new(&format!("/proc/{}", pid)), process.directory);
        assert_ne!(0, process.base_addr);
    }

    #[test]
    fn load_from_id_with_module() {
        let pid = std::process::id();
        let no_module = Process::from_pid(std::process::id()).unwrap();
        let res_process = Process::from_pid_for_module(std::process::id(), "r-xp");
        assert_eq!(true, res_process.is_ok());

        let process = res_process.unwrap();

        assert_eq!(pid, process.pid);
        assert_eq!(Path::new(&format!("/proc/{}", pid)), process.directory);
        assert_ne!(0, process.base_addr);
        assert_eq!(no_module.base_addr, process.base_addr);
    }

    #[test]
    fn set_module_base() {
        let pid = std::process::id();
        let res_process = Process::from_pid_for_module(std::process::id(), "r-xp");
        assert_eq!(true, res_process.is_ok());

        let mut process = res_process.unwrap();

        assert_eq!(pid, process.pid);

        let base = process.base_addr;
        process.set_module_base("r-xp").unwrap();

        assert_eq!(base, process.base_addr);
    }

    #[test]
    fn try_from_u32() {
        let pid = std::process::id();
        let res_process: std::io::Result<Process> = pid.try_into();
        assert_eq!(true, res_process.is_ok());

        let process = res_process.unwrap();

        assert_eq!(pid, process.pid);
        assert_eq!(Path::new(&format!("/proc/{}", pid)), process.directory);
        assert_ne!(0, process.base_addr);
    }

    #[test]
    fn read_u8() {
        let pid = std::process::id();
        let to_read: u8 = 16;
        let mut process = Process::from_pid_for_module(pid, "rw-p").unwrap();

        let mut buffer = [0u8; 1];
        let err = process.read((&to_read as *const _) as u64, &mut buffer);

        assert_eq!(true, err.is_ok());
        assert_eq!(to_read, buffer[0]);
    }

    #[test]
    fn write_u8() {
        let pid = std::process::id();
        let to_read: u8 = 16;
        let to_write: u8 = 0;
        let mut process = Process::from_pid_for_module(pid, "rw-p").unwrap();

        let buffer = [to_write; 1];
        let err = process.write((&to_read as *const _) as u64, &buffer);

        assert_eq!(true, err.is_ok());
        assert_eq!(to_write, to_read);
    }

    #[test]
    fn read_u16() {
        let pid = std::process::id();
        let to_read: u16 = u16::max_value();
        let mut process = Process::from_pid_for_module(pid, "rw-p").unwrap();

        let mut buffer = [0u8; 2];
        let err = process.read((&to_read as *const _) as u64, &mut buffer);

        assert_eq!(true, err.is_ok());
        assert_eq!(to_read, unsafe {
            let buf_ptr = buffer.as_ptr();
            let ptr = buf_ptr as *const u16;
            (*ptr).clone()
        });
    }

    #[test]
    fn write_u16() {
        let pid = std::process::id();
        let to_read: u16 = u16::max_value();
        let to_write: u16 = u16::max_value() / 2;
        let mut process = Process::from_pid_for_module(pid, "rw-p").unwrap();

        let buffer = to_write.to_ne_bytes();
        let err = process.write((&to_read as *const _) as u64, &buffer);

        assert_eq!(true, err.is_ok());
        assert_eq!(to_write, to_read);
    }

    #[test]
    fn read_i16() {
        let pid = std::process::id();
        let to_read: i16 = i16::max_value();
        let mut process = Process::from_pid_for_module(pid, "rw-p").unwrap();

        let mut buffer = [0u8; 2];
        let err = process.read((&to_read as *const _) as u64, &mut buffer);

        assert_eq!(true, err.is_ok());
        assert_eq!(to_read, i16::from_ne_bytes(buffer));
    }

    #[test]
    fn write_i16() {
        let pid = std::process::id();
        let to_read: i16 = i16::max_value();
        let to_write: i16 = i16::min_value() / 2;
        let mut process = Process::from_pid_for_module(pid, "rw-p").unwrap();

        let buffer = to_write.to_ne_bytes();
        let err = process.write((&to_read as *const _) as u64, &buffer);

        assert_eq!(true, err.is_ok());
        assert_eq!(to_write, to_read);
    }
}
