use std::fs;
use std::io;
use std::path::{Path, PathBuf};

/// Search for a program name containing `prg_name` in the given path `dir`. It will return the pid
/// and a path to the process file tree.
pub(crate) fn search(dir: &Path, prg_name: &str) -> io::Result<(u32, PathBuf)> {
    // We need a directory as input:
    if ! dir.is_dir() {
        return Err(io::Error::new(io::ErrorKind::Other, format!("'{}' is not a directory.", dir.display())));
    }

    // We go through all entry in the directory:
    for entry in fs::read_dir(dir)? {
        let entry = entry?;
        let mut path = entry.path();

        // If the entry is not a directory, we do not want it:
        if ! path.is_dir() {
            continue;
        }

        // Extracting the pid if possible or go to the next entry:
        let pid: u32 = match entry.file_name()
            .to_str()
            .ok_or_else(|| io::Error::new(io::ErrorKind::Other, format!("Unable to get a string from the directory '{}'.", path.display())))
            .and_then(|n| n.parse::<u32>().map_err(|e| io::Error::new(io::ErrorKind::Other, e))) {
            Ok(v) => v,
            Err(_) => continue,
        };

        path.push("status");

        // Checking if status exist and is a file, else go to the next entry:
        if ! path.exists() || ! path.is_file() {
            continue;
        }

        // Reading the file:
        let buffer = fs::read_to_string(path)?;

        // Checking if the name contain the program name we search:
        if buffer.contains(prg_name) {
            return Ok((pid, entry.path()));
        }
    }

    Err(io::Error::new(io::ErrorKind::NotFound, format!("No program with name '{}' found.", prg_name)))
}

#[cfg(test)]
mod tests {
    use std::error::Error;
    use super::*;

    #[test]
    fn search_ok() {
        let pid = std::process::id();
        let path = Path::new("/proc/");
        let found = search(path, "hack");

        assert_eq!(true, found.is_ok());

        let mut path = path.to_path_buf();
        path.push(format!("{}", pid));

        let (pid_found, path_found) = found.unwrap();

        assert_eq!(pid, pid_found);
        assert_eq!(path, path_found);
    }

    #[test]
    fn search_wrong_path() {
        let path = Path::new("/tmp/");
        let found = search(path, "hack");

        assert_eq!(true, found.is_err());

        if let Err(err) = found {
            assert_eq!(io::ErrorKind::NotFound, err.kind());
            assert_eq!(true, err.description().contains("No program with name"));
        }
    }

    #[test]
    fn search_not_directory() {
        let path = Path::new("/tmp/screen.png");
        let found = search(path, "hack");

        assert_eq!(true, found.is_err());

        if let Err(err) = found {
            assert_eq!(io::ErrorKind::Other, err.kind());
            assert_eq!(true, err.description().contains("is not a directory."));
        }
    }
}
