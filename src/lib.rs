#![deny(missing_docs)]

//! Library meant to provide tools to read and write other process memory.

mod utils;
pub mod process;
