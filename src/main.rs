use std::io;

use hack::process::Process;

fn main() -> io::Result<()> {
    println!("{:?}", Process::new_for_module("hack-rs", "rw-p"));
    println!("{:?}", Process::new("nvim"));
    println!("{:?}", Process::new_for_module("nvim", "rw-p"));

    let signature = [72, 139, 69, 40, 131, 40, 1, 72];
    let op_code = [144, 144, 144];
    let mut process = Process::new("ac_client")?;

    process.payload(&signature, Some(&op_code), 64, 4)?;

    Ok(())
}
